//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void Tdm::ProdUpd(UnicodeString name, int number) {
	spProdUpd->ParamByName("NAME")->Value = name;
	spProdUpd->ParamByName("NUMBER")->Value = number;

    spProdUpd->ExecProc();
}

void Tdm::PlusItem(UnicodeString name, int number, int price) {
	spPlusItem->ParamByName("NAME")->Value = name;
	spPlusItem->ParamByName("NUMBER")->Value = number;
	spPlusItem->ParamByName("PRICE")->Value = price;

	spPlusItem->ExecProc();
}

void Tdm::DeleteItem(UnicodeString name) {
	spDeleteItem->ParamByName("NAME")->Value = name;

	spDeleteItem->ExecProc();
}