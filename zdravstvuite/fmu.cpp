//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	 tc->TabPosition = TTabPosition::None;
     tc->GotoVisibleTab(tbTable->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buBackClick(TObject *Sender)
{
	tc->GotoVisibleTab(tbTable->Index);
	dm->ProdUpd(laName->Text,StrToInt(laNumber->Text));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	dm->quProduct->Refresh();
	tc->GotoVisibleTab(tbCataloge->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ChangeNumber(TObject *Sender)
{
	if (((TControl*)Sender)->Tag == 0) {
		laNumber->Text = StrToInt(laNumber->Text) - 1;
	}
	else {
		laNumber->Text = StrToInt(laNumber->Text) + 1;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buEdPlClick(TObject *Sender)
{
	tc->GotoVisibleTab(tbPlus->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buPlusClick(TObject *Sender)
{
	if (edName->Text != "" && edNumber->Text != "" && edPrice->Text != "") {
		dm->PlusItem(edName->Text,StrToInt(edNumber->Text),StrToInt(edPrice->Text));
			ShowMessage("Success!");
	}


}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
	tc->GotoVisibleTab(tbTable->Index);
	dm->quProduct->Refresh();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::buDeleteClick(TObject *Sender)
{
	dm->DeleteItem(laName->Text);
	tc->GotoVisibleTab(tbTable->Index);
	dm->quProduct->Refresh();
}
//---------------------------------------------------------------------------

