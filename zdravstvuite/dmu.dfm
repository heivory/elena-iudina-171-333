object dm: Tdm
  OldCreateOrder = False
  Height = 340
  Width = 437
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=D:\'#1103' '#1087#1086#1083#1091#1095#1072#1102' '#1074#1099#1089#1096#1077#1077' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077'\'#1084#1086#1073#1080#1083#1100#1085#1072#1103' '#1088#1072#1079#1088#1072#1073#1086#1090#1082#1072'\'#1088#1077 +
        #1087'5\elena-iudina-171-333\zdravstvuite\db\ZDR.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 40
    Top = 24
  end
  object quProduct: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    sklad.name,'
      '    sklad.number,'
      '    sklad.price'
      'from sklad')
    Left = 136
    Top = 24
    object quProductNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      FixedChar = True
      Size = 30
    end
    object quProductNUMBER: TIntegerField
      FieldName = 'NUMBER'
      Origin = 'NUMBER'
    end
    object quProductPRICE: TIntegerField
      FieldName = 'PRICE'
      Origin = 'PRICE'
    end
  end
  object spProdUpd: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'PROD_UPD'
    Left = 312
    Top = 168
    ParamData = <
      item
        Position = 1
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 30
      end
      item
        Position = 2
        Name = 'NUMBER'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 48
    Top = 280
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 152
    Top = 280
  end
  object spPlusItem: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'NEW_PROCEDURE'
    Left = 232
    Top = 168
    ParamData = <
      item
        Position = 1
        Name = 'PRICE'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'NUMBER'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 30
      end>
  end
  object spDeleteItem: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'PROD_DE'
    Left = 272
    Top = 120
    ParamData = <
      item
        Position = 1
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 30
      end>
  end
end
