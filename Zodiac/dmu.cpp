// ---------------------------------------------------------------------------

#pragma hdrstop

#include "dmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;

// ---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner) : TDataModule(Owner) {
}

void Tdm::FavoriteUpdate(int id) {
	spFavorite->ParamByName("ID")->Value = id;
	spFavorite->ExecProc();
}
// ---------------------------------------------------------------------------
