// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::ZodiacImageClick(TObject *Sender) {
	flag = false;
	zodiac = ((TControl*)Sender)->Tag;
	dm->quForecast->Close();
	dm->quForecast->ParamByName("zodiac")->AsString = IntToStr(zodiac);
	dm->quForecast->Open();
	lbZodiac->Text = dm->quForecastZODIAC_NAME->Value;
	meForecast->Text = dm->quForecastFORECAST_TEXT->Value;
	if (dm->quForecastFAVORITE->Value) {
		buAddToFavorite->Text = "������� �� ����������";
	}
	else {
		buAddToFavorite->Text = "� ���������";
	}
	tc->GotoVisibleTab(tiForecast->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buHoroscopeClick(TObject * Sender) {
	tc->GotoVisibleTab(tiZodiac->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject * Sender) {
	tc->TabPosition = TTabPosition::None;
	tc->GotoVisibleTab(tiMenu->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::allBackClick(TObject *Sender) {
	tc->GotoVisibleTab(tiZodiac->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buAddToFavoriteClick(TObject *Sender) {
	if (buAddToFavorite->Text == "� ���������")
	{
		buAddToFavorite->Text = "������� �� ����������";
	}
	else {
		buAddToFavorite->Text = "� ���������";
	}
	if (flag) {
		dm->FavoriteUpdate(dm->quFavoriteID->Value);
	}
	else {
		dm->FavoriteUpdate(dm->quForecastID->Value);
	}

}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buFavoriteClick(TObject *Sender) {
	dm->quFavorite->Close();
	dm->quFavorite->Open();
	tc->GotoVisibleTab(tiFavorite->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::ListView1ItemClick(TObject * const Sender,
	TListViewItem * const AItem) {
	flag = true;
	lbZodiac->Text = dm->quFavoriteZODIAC_NAME->Value;
	meForecast->Text = dm->quFavoriteFORECAST_TEXT->Value;
	if (dm->quFavoriteFAVORITE->Value) {
		buAddToFavorite->Text = "������� �� ����������";
	}
	else {
		buAddToFavorite->Text = "� ���������";
	}
	tc->GotoVisibleTab(tiForecast->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender) {
	tc->GotoVisibleTab(tiMenu->Index);
}
// ---------------------------------------------------------------------------
