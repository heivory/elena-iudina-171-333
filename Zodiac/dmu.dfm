object dm: Tdm
  OldCreateOrder = False
  Height = 201
  Width = 308
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=D:\'#1103' '#1087#1086#1083#1091#1095#1072#1102' '#1074#1099#1089#1096#1077#1077' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077'\'#1084#1086#1073#1080#1083#1100#1085#1072#1103' '#1088#1072#1079#1088#1072#1073#1086#1090#1082#1072'\'#1088#1077 +
        #1087'5\elena-iudina-171-333\Zodiac\db\ZODIAC.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    Left = 48
    Top = 32
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 24
    Top = 128
  end
  object spFavorite: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'FAVORITE_UPD'
    Left = 232
    Top = 112
    ParamData = <
      item
        Position = 1
        Name = 'ID'
        DataType = ftSmallint
        ParamType = ptInput
      end>
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 120
    Top = 128
  end
  object quForecast: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    forecast.id,'
      '    forecast.zodiac_name,'
      '    forecast.zodiac,'
      '    forecast.favorite,'
      '    forecast.forecast_text'
      'from forecast'
      'where '
      '   ('
      '      (zodiac = :zodiac)'
      '   )'
      'order by Rand()')
    Left = 176
    Top = 32
    ParamData = <
      item
        Name = 'ZODIAC'
        DataType = ftString
        ParamType = ptInput
        Value = '0'
      end>
    object quForecastZODIAC: TSmallintField
      FieldName = 'ZODIAC'
      Origin = 'ZODIAC'
    end
    object quForecastFAVORITE: TBooleanField
      FieldName = 'FAVORITE'
      Origin = 'FAVORITE'
    end
    object quForecastFORECAST_TEXT: TWideStringField
      FieldName = 'FORECAST_TEXT'
      Origin = 'FORECAST_TEXT'
      FixedChar = True
      Size = 300
    end
    object quForecastID: TSmallintField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quForecastZODIAC_NAME: TWideStringField
      FieldName = 'ZODIAC_NAME'
      Origin = 'ZODIAC_NAME'
      FixedChar = True
    end
  end
  object quFavorite: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '     forecast.id,'
      '    forecast.zodiac_name,'
      '    forecast.zodiac,'
      '    forecast.favorite,'
      '    forecast.forecast_text'
      'from forecast'
      'where '
      '   ('
      '      (favorite = true)'
      '   )')
    Left = 136
    Top = 88
    object quFavoriteFORECAST_TEXT: TWideStringField
      FieldName = 'FORECAST_TEXT'
      Origin = 'FORECAST_TEXT'
      FixedChar = True
      Size = 300
    end
    object quFavoriteZODIAC_NAME: TWideStringField
      FieldName = 'ZODIAC_NAME'
      Origin = 'ZODIAC_NAME'
      FixedChar = True
    end
    object quFavoriteID: TSmallintField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quFavoriteZODIAC: TSmallintField
      FieldName = 'ZODIAC'
      Origin = 'ZODIAC'
    end
    object quFavoriteFAVORITE: TBooleanField
      FieldName = 'FAVORITE'
      Origin = 'FAVORITE'
    end
  end
end
