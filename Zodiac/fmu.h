// ---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>

// ---------------------------------------------------------------------------
class TForm1 : public TForm {
__published: // IDE-managed Components
	TTabControl *tc;
	TTabItem *tiZodiac;
	TTabItem *tiForecast;
	TGridPanelLayout *GridPanelLayout1;
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TImage *Image7;
	TImage *Image8;
	TImage *Image9;
	TImage *Image10;
	TImage *Image11;
	TImage *Image12;
	TTabItem *tiMenu;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buHoroscope;
	TButton *buFavorite;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TMemo *meForecast;
	TToolBar *ToolBar1;
	TButton *Button1;
	TButton *buAddToFavorite;
	TLabel *lbZodiac;
	TBindSourceDB *BindSourceDB2;
	TToolBar *ToolBar2;
	TButton *Button3;
	TLabel *Label1;
	TTabItem *tiFavorite;
	TListView *ListView1;
	TToolBar *ToolBar3;
	TButton *Button2;
	TBindSourceDB *BindSourceDB3;
	TLinkListControlToField *LinkListControlToField1;
	TStyleBook *StyleBook1;

	void __fastcall ZodiacImageClick(TObject *Sender);
	void __fastcall buHoroscopeClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall allBackClick(TObject *Sender);
	void __fastcall buAddToFavoriteClick(TObject *Sender);
	void __fastcall buFavoriteClick(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender,
		TListViewItem * const AItem);
	void __fastcall Button3Click(TObject *Sender);

private: // User declarations
	int zodiac;

	bool flag = false;

	UnicodeString type;

public: // User declarations
	__fastcall TForm1(TComponent* Owner);
};

// ---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
// ---------------------------------------------------------------------------
#endif
