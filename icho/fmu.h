//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tbMenu;
	TTabItem *tbMovie;
	TTabItem *tbGanr;
	TButton *buMovie;
	TButton *buGanr;
	TToolBar *ToolBar1;
	TButton *Button1;
	TListView *ListView1;
	TToolBar *ToolBar2;
	TButton *Button2;
	TListView *ListView2;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TLabel *Label1;
	TLabel *Label2;
	TBindSourceDB *BindSourceDB2;
	TLinkListControlToField *LinkListControlToField2;
	TTabItem *tbMovieAbout;
	TToolBar *ToolBar3;
	TButton *Button3;
	TListView *ListView3;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *laTitle;
	TLabel *laDesc;
	TTabItem *tbGanrM;
	TToolBar *ToolBar4;
	TListView *ListView4;
	TBindSourceDB *BindSourceDB3;
	TLinkListControlToField *LinkListControlToField3;
	TButton *Button4;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buMovieClick(TObject *Sender);
	void __fastcall buGanrClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall ListView2ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall Button4Click(TObject *Sender);


private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
