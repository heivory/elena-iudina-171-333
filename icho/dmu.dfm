object dm: Tdm
  OldCreateOrder = False
  Height = 273
  Width = 340
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=D:\'#1103' '#1087#1086#1083#1091#1095#1072#1102' '#1074#1099#1089#1096#1077#1077' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077'\'#1084#1086#1073#1080#1083#1100#1085#1072#1103' '#1088#1072#1079#1088#1072#1073#1086#1090#1082#1072'\'#1088#1077 +
        #1087'5\elena-iudina-171-333\icho\db\NEEEW.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 16
    Top = 16
  end
  object quMovie: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    movie.title,'
      '    movie.description'
      'from movie')
    Left = 160
    Top = 16
    object quMovieTITLE: TWideStringField
      FieldName = 'TITLE'
      Origin = 'TITLE'
      FixedChar = True
      Size = 30
    end
    object quMovieDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Origin = 'DESCRIPTION'
      FixedChar = True
      Size = 50
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 24
    Top = 224
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 128
    Top = 224
  end
  object quGanr: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select'
      '    ganr.id,'
      '    ganr.name'
      'from ganr')
    Left = 216
    Top = 16
    object quGanrID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quGanrNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      FixedChar = True
      Size = 30
    end
  end
  object quChoto: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select'
      ' movie.id,'
      ' movie.title,'
      ' movie.description,'
      ' movie.ganr_id'
      'from movie'
      'where movie.ganr_id = :ganr_id;')
    Left = 272
    Top = 16
    ParamData = <
      item
        Name = 'GANR_ID'
        DataType = ftString
        ParamType = ptInput
        Value = '1'
      end>
    object quChotoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quChotoTITLE: TWideStringField
      FieldName = 'TITLE'
      Origin = 'TITLE'
      FixedChar = True
      Size = 30
    end
    object quChotoDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Origin = 'DESCRIPTION'
      FixedChar = True
      Size = 50
    end
    object quChotoGANR_ID: TIntegerField
      FieldName = 'GANR_ID'
      Origin = 'GANR_ID'
    end
  end
end
