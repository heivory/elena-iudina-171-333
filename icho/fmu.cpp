//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	tc->TabPosition = TTabPosition::None;
	tc->GotoVisibleTab(tbMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buMovieClick(TObject *Sender)
{
	tc->GotoVisibleTab(tbMovie->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buGanrClick(TObject *Sender)
{
    tc->GotoVisibleTab(tbGanr->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	tc->GotoVisibleTab(tbMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	tc->GotoVisibleTab(tbMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	dm->quMovie->Refresh();
	laTitle->Text = dm->quMovieTITLE->Value;
	laDesc->Text = dm->quMovieDESCRIPTION->Value;
//	laGanr->Text = dm->quGanrNAME->Value;
	tc->GotoVisibleTab(tbMovieAbout->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	tc->GotoVisibleTab(tbMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ListView2ItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	dm->quChoto->Close();
	dm->quChoto->ParamByName("GANR_ID")->Value = IntToStr(dm->quGanrID->Value);
	dm->quChoto->Open();
	tc->GotoVisibleTab(tbGanrM->Index);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
	tc->GotoVisibleTab(tbMenu->Index);
}
//---------------------------------------------------------------------------

