object dm: Tdm
  OldCreateOrder = False
  Height = 239
  Width = 334
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\VD\Documents\Embarcadero\Studio\Projects\Notes' +
        '\Notes.db'
      'DriverID=SQLite')
    LoginPrompt = False
    AfterConnect = FDConnection1AfterConnect
    BeforeConnect = FDConnection1BeforeConnect
    Left = 48
    Top = 16
  end
  object taNotes: TFDTable
    Connection = FDConnection1
    UpdateOptions.UpdateTableName = 'Notes'
    TableName = 'Notes'
    Left = 48
    Top = 96
    object taNotesCaption: TStringField
      FieldName = 'Caption'
      Origin = 'Caption'
      Required = True
      Size = 50
    end
    object taNotesPriority: TSmallintField
      FieldName = 'Priority'
      Origin = 'Priority'
      Required = True
    end
    object taNotesDetail: TStringField
      FieldName = 'Detail'
      Origin = 'Detail'
      Size = 500
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 152
    Top = 104
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 200
    Top = 32
  end
end
