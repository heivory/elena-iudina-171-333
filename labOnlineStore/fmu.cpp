//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "fruCategory.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}

void TForm1::ReloadCategoryList()
{
	dm->Categoty->First();
	while(!dm->quCategory->Eof) {
		TfrCategory *x = new TfrCategory (qlCategory);
		x->Parent = glCategory;
		x->Align = TAlignLayout::Client;
		x->Name = "frCategory"+IntToStr(dm->quCategoryID->Value);
		x->la->Text = dm ->quCategoryNAME->Value;
		x->im->Bitmap->Assign(dm->quCategoryIMAGE);
        x->Tag = dm->quCategoryID->Value;
		dm->quCategory->Next();
	}
	glCategory->RecalcSize();
}
//---------------------------------------------------------------------------
