//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TabControl1;
	TTabItem *tiMenu;
	TTabItem *tiCategoryList;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *tiFeedback;
	TLayout *Layout1;
	TButton *Button1;
	TLabel *Label1;
	TScrollBox *ScrollBox1;
	TLabel *Label2;
	TEdit *Edit1;
	TLabel *Label3;
	TEdit *Edit2;
	TLabel *Label4;
	TEdit *Edit3;
	TLabel *Label6;
	TMemo *Memo1;
	TLabel *Label5;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buCardProduct;
	TButton *BuCategory;
	TButton *Button4;
	TButton *Button5;
	TLayout *Layout2;
	TLayout *Layout3;
	TButton *Button2;
	TLabel *Label7;
	TScrollBox *ScrollBox2;
	TGridLayout *GridLayout1;
private:	// User declarations
    void ReloadCategoryList();
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
