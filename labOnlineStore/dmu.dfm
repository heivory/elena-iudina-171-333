object DataModule2: TDataModule2
  OldCreateOrder = False
  Height = 312
  Width = 411
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\Users\VD\Desktop\labOnlineStore\db\ONLINESTORE.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'DriverID=FB')
    Connected = True
    Left = 352
    Top = 24
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 32
    Top = 80
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 24
    Top = 16
  end
  object spFeedbackIns: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'FEEDBACK_INS'
    Left = 352
    Top = 112
    ParamData = <
      item
        Position = 1
        Name = 'FIO'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 2
        Name = 'PHONE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 15
      end
      item
        Position = 3
        Name = 'EMAIL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 4
        Name = 'NOTE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 4000
      end>
  end
end
