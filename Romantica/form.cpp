// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {
	tabCtrl->First();
	tabCtrl->TabPosition = TTabPosition::None;

	steps = {
		{1}, {2}, {3, 4}, {5}, {5}, {6}, {7, 8}, {9}, {9}, {10}, {11}, {-2, 12},
		{13}, {-2, 14}, {15}, {16}, {-1}};
	text = {
		L"���, ������� � �������� �515. ��� ����� ��, �������!",
		L"������ ����, ��������!", L"������������� � ��������������?",
		L"� ��� ������������, �����!", L"���, �����, ���������...",
		L"�� ������ �� ����� �� ����� � �����, �����?", L"����� ����� � �����?",
		L"�������� ����� �����, �����!", L"�� � ����� �� ��� ������ ���������?",
		L"�����, ���������, ������ �����.",
		L"*����� �����* �, ���. ��� ����� ������� ����� �� ���� ���������. � ���� �� ����� ���!",
		L"����� ��� ���� �� �������� ������ � ����� ����� �� ���������?",
		L"���, �� ������� ��� ������ �� �������! �������� ������.",
		L"���, � �������� ����� ������, �� ������ �� �����. ����� ��������������� ����������?",
		L"���, �� ������� � ������������ �� ����! ����� �������.",
		L"��, ��� ��������� ������, ��, � ��������, ���������, �������!",
		L"�������!"};

	sprites = {
		{4, -1}, {-1, 6}, {4, -1}, {-1, 6}, {-1, 6}, {-1, 6}, {4, -1}, {-1, 7},
		{-1, 7}, {-1, 6}, {4, -1}, {4, -1}, {4, -1}, {4, -1}, {4, -1}, {-1, 6},
		{4, -1}};

	stepIndex = 0;
	textLbl->Text = text[stepIndex];
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::choiceBtnClick(TObject *Sender) {
	if (rightBtn->Text == L"������ ���� �����") {
		stepIndex = 0;
		nextStep(stepIndex, 1);
	}
	if (stepIndex != -1) {
		nextStep(stepIndex, ((TControl*)Sender)->Tag);
	}

}

void TForm1::nextStep(int index, int tag) {
	if (tag == 0) {
		stepIndex = steps[index][1];
	}
	else {
		stepIndex = steps[index][0];
	}

	if (stepIndex != -1) {
		if (stepIndex == -2) {
			textLbl->Text = L"Game Over!";
			rightBtn->Text = L"������ ���� �����";
			leftBtn->Text = L"";
			leftBtn->Enabled = false;
			return;
		}
		textLbl->Text = text[stepIndex];
		if (steps[stepIndex].size() > 1) {
			leftBtn->Enabled = true;
			rightBtn->Text = L"��";
			leftBtn->Text = L"���";
		}
		else {
			leftBtn->Enabled = false;
			leftBtn->Text = L"";
			rightBtn->Text = L"�����";
		}

		char1Glyph->ImageIndex = sprites[stepIndex][0];
		char2Glyph->ImageIndex = sprites[stepIndex][1];
	}
	else {
		textLbl->Text = L"����������� �������...";

		rightBtn->Text = L"������ ���� �����";
		leftBtn->Text = L"";
		leftBtn->Enabled = false;
	}
}

// ---------------------------------------------------------------------------
