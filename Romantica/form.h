// ---------------------------------------------------------------------------

#ifndef formH
#define formH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <System.ImageList.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Ani.hpp>
#include <vector>

using namespace std;

// ---------------------------------------------------------------------------
class TForm1 : public TForm {
__published: // IDE-managed Components
	TToolBar *ToolBar1;
	TGridPanelLayout *GridPanelLayout1;
	TButton *leftBtn;
	TButton *rightBtn;
	TTabControl *tabCtrl;
	TTabItem *TabItem1;
	TImageList *background;
	TGlyph *backgroundImg;
	TLabel *textLbl;
	TImageList *char1;
	TImageList *char2;
	TGlyph *char1Glyph;
	TGlyph *char2Glyph;

	void __fastcall FormCreate(TObject *Sender);
	void __fastcall choiceBtnClick(TObject *Sender);

private: // User declarations
	void nextStep(int index, int tag);

	int stepIndex;
	int backgroundIndex;

	vector < vector < int>> sprites;

	vector<UnicodeString>text;

	vector < vector < int >> steps;

public: // User declarations
	__fastcall TForm1(TComponent* Owner);
};

// ---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
// ---------------------------------------------------------------------------
#endif
