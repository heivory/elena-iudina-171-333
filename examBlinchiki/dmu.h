//---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.IBBase.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TFDConnection *FDConnection1;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDPhysFBDriverLink *FDPhysFBDriverLink1;
	TFDQuery *quBlini;
	TIntegerField *quBliniID;
	TIntegerField *quBliniCATEGORY_ID;
	TWideStringField *quBliniNAME;
	TBlobField *quBliniIMAGE;
	TIntegerField *quBliniGRAM;
	TIntegerField *quBliniKCAL;
	TFloatField *quBliniPRICE;
	TWideStringField *quBliniNOTE;
	TIntegerField *quBliniSTICKER;
	TFDStoredProc *spProcKorz;
	TFDQuery *quOrdering;
	TFDQuery *quOrderingList;
	TIntegerField *quOrderingID;
	TSQLTimeStampField *quOrderingCREATE_DATETIME;
	TWideStringField *quOrderingCLIENT_FIO;
	TWideStringField *quOrderingCLIENT_TEL;
	TWideStringField *quOrderingCLIENT_EMAIL;
	TWideStringField *quOrderingCLIENT_ADDRESS;
	TDateField *quOrderingDELIVERY_DATE;
	TTimeField *quOrderingDELIVERY_TIME;
	TFloatField *quOrderingDELIVERY_AMOUNT;
	TWideStringField *quOrderingDELIVERY_NOTE;
	TIntegerField *quOrderingSTATUS;
	TWideStringField *quOrderingSTATUS_NOTE;
	TIntegerField *quOrderingListID;
	TIntegerField *quOrderingListORDER_ID;
	TIntegerField *quOrderingListPRODUCT_ID;
	TFloatField *quOrderingListPRODUCT_PRICE;
	TIntegerField *quOrderingListQUANTITY;
	TFDStoredProc *spProcMessage;
	TFDQuery *quMessage;
	TIntegerField *quMessageID;
	TWideStringField *quMessageNAME;
	TIntegerField *quMessageTEL;
	TWideStringField *quMessageEMAIL;
	TWideStringField *quMessageMESSAGE;
	TFDStoredProc *spProcNewOrder;
private:	// User declarations
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);

void ProcKorz(int order_id, int product_id, int quantity, double product_price);
void ProcMessage(UnicodeString name, int tel, UnicodeString email, UnicodeString message);
void ProcNewOrder(int id, UnicodeString client_email, UnicodeString client_tel,
UnicodeString client_fio, UnicodeString delivery_amount, UnicodeString client_address,
UnicodeString delivery_note, UnicodeString delivery_date, UnicodeString delivery_time);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
