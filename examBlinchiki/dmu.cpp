//---------------------------------------------------------------------------


#pragma hdrstop

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------
void Tdm::ProcKorz(int order_id, int product_id, int quantity, double product_price) {
	spProcKorz->ParamByName("ORDER_ID")->Value = order_id;
	spProcKorz->ParamByName("PRODUCT_ID")->Value = product_id;
	spProcKorz->ParamByName("QUANTITY")->Value = quantity;
	spProcKorz->ParamByName("PRODUCT_PRICE")->Value = product_price;

    spProcKorz->ExecProc();
}

void Tdm::ProcMessage(UnicodeString name, int tel, UnicodeString email, UnicodeString message) {
	spProcMessage->ParamByName("NAME")->Value = name;
	spProcMessage->ParamByName("TEL")->Value = tel;
	spProcMessage->ParamByName("EMAIL")->Value = email;
	spProcMessage->ParamByName("MESSAGE")->Value = message;

    spProcMessage->ExecProc();
}

void Tdm::ProcNewOrder(int id, UnicodeString client_email, UnicodeString client_tel,
UnicodeString client_fio, UnicodeString delivery_amount, UnicodeString client_address,
UnicodeString delivery_note, UnicodeString delivery_date, UnicodeString delivery_time) {
    spProcNewOrder->ParamByName("ID")->Value = id;
	spProcNewOrder->ParamByName("CLIENT_FIO")->Value = client_fio;
	spProcNewOrder->ParamByName("CLIENT_EMAIL")->Value = client_email;
	spProcNewOrder->ParamByName("CLIENT_TEL")->Value = client_tel;
	spProcNewOrder->ParamByName("DELIVERY_AMOUNT")->Value = delivery_amount;
	spProcNewOrder->ParamByName("CLIENT_ADDRESS")->Value = client_address;
	spProcNewOrder->ParamByName("DELIVERY_NOTE")->Value = (double)StrToInt(delivery_note);
	spProcNewOrder->ParamByName("DELIVERY_DATE")->Value = StrToDate(delivery_date);
	spProcNewOrder->ParamByName("DELIVERY_TIME")->Value = StrToTime(delivery_time);

    spProcNewOrder->ExecProc();
}
