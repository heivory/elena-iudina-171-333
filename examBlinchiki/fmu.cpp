// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
#include "frame.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;

// ---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::buBliniClick(TObject *Sender) {
	tc->GotoVisibleTab(tbBlini->Index);
}

// ---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender) {
	tc->TabPosition = TTabPosition::None;
	tc->GotoVisibleTab(tbMain->Index);
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::ListView1ItemClick(TObject * const Sender,
	TListViewItem * const AItem)

{
	dm->quBlini->Refresh();
	tc->GotoVisibleTab(tbAboutBlin->Index);
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buAboutBlinBackClick(TObject *Sender) {
	tc->GotoVisibleTab(tbBlini->Index);
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::ChangeClick(TObject *Sender) {
	if (((TControl*)Sender)->Tag == 0) {
		if (StrToInt(laNumber->Text) > 0) {

			laNumber->Text = StrToInt(laNumber->Text) - 1;

		}
	}
	else {
		laNumber->Text = StrToInt(laNumber->Text) + 1;
	}
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::Button1Click(TObject *Sender) {
	tc->GotoVisibleTab(tbMain->Index);
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buContactsClick(TObject *Sender) {
	tc->GotoVisibleTab(tbCont->Index);
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buKorzClick(TObject *Sender) {
	tc->GotoVisibleTab(tbKorz->Index);
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buIntoKorzClick(TObject *Sender) {

	TFrame1 * x = new TFrame1(glKorzina);
	x->Parent = glKorzina;
	x->Align = TAlignLayout::Client;
	x->Name = "frame" + IntToStr(dm->quBliniID->Value);
	x->laOFName->Text = dm->quBliniNAME->Value;
	x->imFrame->Bitmap->Assign(dm->quBliniIMAGE);
	x->laOFPrice->Text = dm->quBliniPRICE->Value;
	x->laOFQuantity->Text = laNumber->Text;

	glKorzina->RecalcSize();

	tc->GotoVisibleTab(tbKorz->Index);

}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buAddMessageClick(TObject *Sender) {
	tc->GotoVisibleTab(tbAddMessage->Index);
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buObrClick(TObject *Sender) {
	tc->GotoVisibleTab(tbMessage->Index);
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buAddMessageNowClick(TObject *Sender) {
	if (edName->Text != "" && edTel->Text != "" && edEmail->Text != "" &&
		edMessage->Text != "") {
		dm->ProcMessage(edName->Text, StrToInt(edTel->Text), edEmail->Text,
			edMessage->Text);
		ShowMessage("����� ��������. �������!");
	}
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buBackAddMessageClick(TObject *Sender) {
	dm->quMessage->Refresh();
	tc->GotoVisibleTab(tbMessage->Index);
}
// ---------------------------------------------------------------------------

void __fastcall Tfm::buOformClick(TObject *Sender) {

	dm->ProcNewOrder(StrToInt(edID->Text), edOEmail->Text, edOTel->Text, edOName->Text, edOPay->Text,
		edOAddress->Text, edOEct->Text, edODate->Text, edOTime->Text);

}
// ---------------------------------------------------------------------------
void __fastcall Tfm::buVikClick(TObject *Sender)
{
    ShowMessage("������ ������� ��������� � ����������!");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buTemaClick(TObject *Sender)
{
	ShowMessage("������ ������� ��������� � ����������!");
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buQuickClick(TObject *Sender)
{
    tc->GotoVisibleTab(tbOform->Index);
}
//---------------------------------------------------------------------------

void __fastcall Tfm::buOformitClick(TObject *Sender)
{
	tc->GotoVisibleTab(tbOform->Index);
}
//---------------------------------------------------------------------------

