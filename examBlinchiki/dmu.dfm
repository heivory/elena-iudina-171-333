object dm: Tdm
  OldCreateOrder = False
  Height = 340
  Width = 397
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=D:\'#1103' '#1087#1086#1083#1091#1095#1072#1102' '#1074#1099#1089#1096#1077#1077' '#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077'\'#1084#1086#1073#1080#1083#1100#1085#1072#1103' '#1088#1072#1079#1088#1072#1073#1086#1090#1082#1072'\'#1088#1077 +
        #1087'5\elena-iudina-171-333\examBlinchiki\db\BLINCHIKI.fdb'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 32
    Top = 16
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 48
    Top = 280
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 152
    Top = 280
  end
  object quBlini: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    product.id,'
      '    product.category_id,'
      '    product.name,'
      '    product.image,'
      '    product.gram,'
      '    product.kcal,'
      '    product.price,'
      '    product.note,'
      '    product.sticker'
      'from product'
      'order by product.category_id, product.name')
    Left = 144
    Top = 16
    object quBliniID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quBliniCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Origin = 'CATEGORY_ID'
      Required = True
    end
    object quBliniNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 100
    end
    object quBliniIMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
    object quBliniGRAM: TIntegerField
      FieldName = 'GRAM'
      Origin = 'GRAM'
    end
    object quBliniKCAL: TIntegerField
      FieldName = 'KCAL'
      Origin = 'KCAL'
    end
    object quBliniPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      Required = True
    end
    object quBliniNOTE: TWideStringField
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 4000
    end
    object quBliniSTICKER: TIntegerField
      FieldName = 'STICKER'
      Origin = 'STICKER'
    end
  end
  object spProcKorz: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'NEW_PROCEDURE'
    Left = 296
    Top = 120
    ParamData = <
      item
        Position = 1
        Name = 'QUANTITY'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'PRODUCT_PRICE'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'PRODUCT_ID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'ORDER_ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object quOrdering: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    ordering.id,'
      '    ordering.create_datetime,'
      '    ordering.client_fio,'
      '    ordering.client_tel,'
      '    ordering.client_email,'
      '    ordering.client_address,'
      '    ordering.delivery_date,'
      '    ordering.delivery_time,'
      '    ordering.delivery_amount,'
      '    ordering.delivery_note,'
      '    ordering.status,'
      '    ordering.status_note'
      'from ordering'
      'order by ordering.id desc')
    Left = 200
    Top = 16
    object quOrderingID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quOrderingCREATE_DATETIME: TSQLTimeStampField
      FieldName = 'CREATE_DATETIME'
      Origin = 'CREATE_DATETIME'
      Required = True
    end
    object quOrderingCLIENT_FIO: TWideStringField
      FieldName = 'CLIENT_FIO'
      Origin = 'CLIENT_FIO'
      Size = 70
    end
    object quOrderingCLIENT_TEL: TWideStringField
      FieldName = 'CLIENT_TEL'
      Origin = 'CLIENT_TEL'
      Size = 15
    end
    object quOrderingCLIENT_EMAIL: TWideStringField
      FieldName = 'CLIENT_EMAIL'
      Origin = 'CLIENT_EMAIL'
      Size = 70
    end
    object quOrderingCLIENT_ADDRESS: TWideStringField
      FieldName = 'CLIENT_ADDRESS'
      Origin = 'CLIENT_ADDRESS'
      Size = 150
    end
    object quOrderingDELIVERY_DATE: TDateField
      FieldName = 'DELIVERY_DATE'
      Origin = 'DELIVERY_DATE'
    end
    object quOrderingDELIVERY_TIME: TTimeField
      FieldName = 'DELIVERY_TIME'
      Origin = 'DELIVERY_TIME'
    end
    object quOrderingDELIVERY_AMOUNT: TFloatField
      FieldName = 'DELIVERY_AMOUNT'
      Origin = 'DELIVERY_AMOUNT'
    end
    object quOrderingDELIVERY_NOTE: TWideStringField
      FieldName = 'DELIVERY_NOTE'
      Origin = 'DELIVERY_NOTE'
      Size = 4000
    end
    object quOrderingSTATUS: TIntegerField
      FieldName = 'STATUS'
      Origin = 'STATUS'
      Required = True
    end
    object quOrderingSTATUS_NOTE: TWideStringField
      FieldName = 'STATUS_NOTE'
      Origin = 'STATUS_NOTE'
      Size = 4000
    end
  end
  object quOrderingList: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    ordering_list.id,'
      '    ordering_list.order_id,'
      '    ordering_list.product_id,'
      '    ordering_list.product_price,'
      '    ordering_list.quantity'
      'from ordering_list,'
      'ordering'
      'order by ordering_list.id')
    Left = 272
    Top = 16
    object quOrderingListID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quOrderingListORDER_ID: TIntegerField
      FieldName = 'ORDER_ID'
      Origin = 'ORDER_ID'
      Required = True
    end
    object quOrderingListPRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Origin = 'PRODUCT_ID'
      Required = True
    end
    object quOrderingListPRODUCT_PRICE: TFloatField
      FieldName = 'PRODUCT_PRICE'
      Origin = 'PRODUCT_PRICE'
      Required = True
    end
    object quOrderingListQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Origin = 'QUANTITY'
      Required = True
    end
  end
  object spProcMessage: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'MESSAGE_PROC'
    Left = 296
    Top = 176
    ParamData = <
      item
        Position = 1
        Name = 'MESSAGE'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 1000
      end
      item
        Position = 2
        Name = 'EMAIL'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 50
      end
      item
        Position = 3
        Name = 'TEL'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 50
      end>
  end
  object quMessage: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    "MESSAGE".id,'
      '    "MESSAGE".name,'
      '    "MESSAGE".tel,'
      '    "MESSAGE".email,'
      '    "MESSAGE"."MESSAGE"'
      'from "MESSAGE"'
      'order by "MESSAGE".id')
    Left = 144
    Top = 80
    object quMessageID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quMessageNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      FixedChar = True
      Size = 50
    end
    object quMessageTEL: TIntegerField
      FieldName = 'TEL'
      Origin = 'TEL'
    end
    object quMessageEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      FixedChar = True
      Size = 50
    end
    object quMessageMESSAGE: TWideStringField
      FieldName = 'MESSAGE'
      Origin = '"MESSAGE"'
      FixedChar = True
      Size = 1000
    end
  end
  object spProcNewOrder: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'NEW_ORDER'
    Left = 296
    Top = 232
    ParamData = <
      item
        Position = 1
        Name = 'CLIENT_EMAIL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 2
        Name = 'CLIENT_TEL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 15
      end
      item
        Position = 3
        Name = 'CLIENT_FIO'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 4
        Name = 'DELIVERY_AMOUNT'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Position = 5
        Name = 'CLIENT_ADDRESS'
        DataType = ftWideString
        ParamType = ptInput
        Size = 150
      end
      item
        Position = 6
        Name = 'DELIVERY_NOTE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 4000
      end
      item
        Position = 7
        Name = 'DELIVERY_DATE'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 8
        Name = 'DELIVERY_TIME'
        DataType = ftTime
        ParamType = ptInput
      end>
  end
end
