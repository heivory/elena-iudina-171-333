//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Edit.hpp>

//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tbMain;
	TImage *Image1;
	TButton *buBlini;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buVik;
	TButton *buKorz;
	TButton *buObr;
	TButton *buYsl;
	TButton *buTema;
	TButton *buContacts;
	TTabItem *tbBlini;
	TToolBar *ToolBar1;
	TButton *Button1;
	TListView *ListView1;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TTabItem *tbAboutBlin;
	TToolBar *ToolBar2;
	TButton *buAboutBlinBack;
	TLabel *Label1;
	TLabel *laName;
	TLabel *Label2;
	TLabel *laGram;
	TLabel *Label4;
	TLabel *laKcal;
	TLabel *Label6;
	TLabel *laPrice;
	TLabel *Label3;
	TLabel *laAbout;
	TImage *imBlin;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkPropertyToField *LinkPropertyToFieldText5;
	TLabel *Label5;
	TLabel *laNumber;
	TButton *buMinus;
	TButton *buPlus;
	TToolBar *ToolBar3;
	TButton *buIntoKorz;
	TButton *buQuick;
	TTabItem *tbCont;
	TToolBar *ToolBar4;
	TButton *Button2;
	TLabel *Label7;
	TLabel *Label8;
	TLabel *Label9;
	TImage *Image2;
	TTabItem *tbKorz;
	TTabItem *tbMessage;
	TToolBar *ToolBar5;
	TButton *Button3;
	TLabel *Label10;
	TListView *ListView2;
	TButton *buAddMessage;
	TBindSourceDB *BindSourceDB2;
	TLinkListControlToField *LinkListControlToField2;
	TTabItem *tbAddMessage;
	TToolBar *ToolBar6;
	TButton *buBackAddMessage;
	TLabel *Label11;
	TLabel *Label12;
	TLabel *Label13;
	TLabel *Label14;
	TLabel *Label15;
	TEdit *edName;
	TEdit *edTel;
	TEdit *edEmail;
	TEdit *edMessage;
	TButton *buAddMessageNow;
	TToolBar *ToolBar7;
	TButton *Button4;
	TLabel *Label16;
	TLabel *Label17;
	TLabel *Label18;
	TStyleBook *StyleBook1;
	TTabItem *tbYsl;
	TToolBar *ToolBar8;
	TButton *Button5;
	TLabel *Label19;
	TImage *Image3;
	TLabel *Label20;
	TGridLayout *glKorzina;
	TButton *buOformit;
	TTabItem *tbOform;
	TToolBar *ToolBar9;
	TButton *Button7;
	TLabel *Label21;
	TLabel *Label22;
	TLabel *Label23;
	TLabel *Label24;
	TLabel *Label25;
	TLabel *Label26;
	TLabel *Label27;
	TLabel *Label28;
	TLabel *Label29;
	TEdit *edOName;
	TEdit *edOTel;
	TEdit *edOEmail;
	TEdit *edOAddress;
	TEdit *edODate;
	TEdit *edOTime;
	TEdit *edOPay;
	TEdit *edOEct;
	TButton *buOform;
	TLabel *Label30;
	TEdit *edID;
	void __fastcall buBliniClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buAboutBlinBackClick(TObject *Sender);
	void __fastcall ChangeClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall buContactsClick(TObject *Sender);
	void __fastcall buKorzClick(TObject *Sender);
	void __fastcall buIntoKorzClick(TObject *Sender);
	void __fastcall buAddMessageClick(TObject *Sender);
	void __fastcall buObrClick(TObject *Sender);
	void __fastcall buAddMessageNowClick(TObject *Sender);
	void __fastcall buBackAddMessageClick(TObject *Sender);
	void __fastcall buOformClick(TObject *Sender);
	void __fastcall buVikClick(TObject *Sender);
	void __fastcall buTemaClick(TObject *Sender);
	void __fastcall buQuickClick(TObject *Sender);
	void __fastcall buOformitClick(TObject *Sender);


private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
	vector < vector < double >> bucket;
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
