//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tbMenu;
	TTabItem *tbTimer;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TTimer *tm;
	TLabel *laTimer;
	TTabItem *tbRandom;
	TLabel *laRandom;
	TToolBar *ToolBar1;
	TButton *Button5;
	TLabel *Label1;
	TButton *Button6;
	TEdit *edRandom;
	TButton *buRandom;
	TTabItem *tbImages;
	TGridPanelLayout *GridPanelLayout2;
	TGlyph *Glyph1;
	TGlyph *Glyph2;
	TGlyph *Glyph3;
	TGlyph *Glyph4;
	TImageList *ImageList1;
	TTabItem *tbSum;
	TMemo *memo;
	TEdit *edSum1;
	TEdit *edSum2;
	TButton *buSum;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall buRandomClick(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
		  TShiftState Shift);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall buSumClick(TObject *Sender);
private:	// User declarations
	TDateTime FTimeStart;
	TGlyph *FListGlyph[4];
	void StartSum(int a,int b);
    int Sum(int a,int b);
	int cSum;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//extern DELPHI_PACKAGE System::UnicodeString __fastcall IntToStr(int Value)/* overload */;
//extern DELPHI_PACKAGE int __fastcall StrToInt(const System::UnicodeString S)/* overload */;
//---------------------------------------------------------------------------
#endif
