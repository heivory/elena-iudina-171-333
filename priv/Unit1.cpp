// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {
	tc->TabPosition = TTabPosition::None;
	tc->GotoVisibleTab(tbMenu->Index);

	for (int i = 0; i <= 3; i++) {
		FListGlyph[i] =
			(TGlyph*)(this->FindComponent("Glyph" + IntToStr(i + 1)));
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender) {
	tc->GotoVisibleTab(tbTimer->Index);
	FTimeStart = StrToTime(laTimer->Text);
	tm->Enabled = true;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::tmTimer(TObject *Sender) {
	laTimer->Text = TimeToStr(FTimeStart - StrToTime("00:00:01"));
	FTimeStart = StrToTime(laTimer->Text);
	if (FTimeStart == StrToTime("00:00:00")) {
		tm->Enabled = false;
		ShowMessage("Game over");
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender) {
	tc->GotoVisibleTab(tbRandom->Index);
	laRandom->Text = Random(9);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender) {
	tc->GotoVisibleTab(tbMenu->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buRandomClick(TObject *Sender) {
	if (laRandom->Text == edRandom->Text) {
		ShowMessage("Success!");
	}
	else {
		ShowMessage("Failed!");
	}

	laRandom->Text = Random(9);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key,
	System::WideChar &KeyChar, TShiftState Shift) {

	if (Key == vkRight) {
		for (int i = 0; i <= 3; i++) {
			if (FListGlyph[i]->ImageIndex == 0 && (i == 0 || i == 2)) {
				FListGlyph[i]->ImageIndex = -1;
				FListGlyph[i + 1]->ImageIndex = 0;
				break;
			 }
		}
	}

	if (Key == vkLeft) {
		for (int i = 0; i <= 3; i++) {
			if (FListGlyph[i]->ImageIndex == 0 && (i == 1 || i == 3)) {
				FListGlyph[i]->ImageIndex = -1;
				FListGlyph[i - 1]->ImageIndex = 0;
				break;
			}
		 }
	}

	if (Key == vkUp) {
		for (int i = 0; i <= 3; i++) {
			if (FListGlyph[i]->ImageIndex == 0 && (i == 2 || i == 3)) {

			}
		}
	}

	if (Key == vkDown) {
		for (int i = 0; i <= 3; i++) {
			if (FListGlyph[i]->ImageIndex == 0 && (i == 0 || i == 1)) {
			}

		}

	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	tc->GotoVisibleTab(tbImages->Index);
}
//---------------------------------------------------------------------------

void TForm1::StartSum(int a,int b) {

	memo->Lines->Add((AnsiString(a) + " + " + AnsiString(b) + " = "
	+ AnsiString(cSum)));
}
void __fastcall TForm1::Button4Click(TObject *Sender)
{
	tc->GotoVisibleTab(tbSum->Index);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::buSumClick(TObject *Sender)
{
//	cSum = StrToInt(edSum1->Text) + StrToInt(edSum2->Text);
//	StartSum(StrToInt(edSum1->Text),StrToInt(edSum2->Text));

	memo->Lines->Add(edSum1->Text + " + " + edSum2->Text + " = "
	+ Sum(StrToInt(edSum1->Text),StrToInt(edSum2->Text)));
}
//---------------------------------------------------------------------------
int TForm1::Sum(int a,int b) {

	return a+b;

}
