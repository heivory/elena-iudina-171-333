//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Filter.Effects.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TBlurEffect *BlurEffect1;
	TBevelEffect *BevelEffect1;
	TBevelEffect *BevelEffect2;
	TBlurEffect *BlurEffect2;
	TRippleEffect *RippleEffect1;
	TRippleEffect *RippleEffect2;
	TSwirlEffect *SwirlEffect1;
	TSwirlEffect *SwirlEffect2;
	TTilerEffect *TilerEffect1;
	TTilerEffect *TilerEffect2;
	THueAdjustEffect *HueAdjustEffect1;
	THueAdjustEffect *HueAdjustEffect2;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
