//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.NumberBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buAbout;
	TLabel *laCaption;
	TLayout *ly;
	TEdit *edPassword;
	TButton *buPassword;
	TCheckBox *ckUpper;
	TCheckBox *ckLower;
	TCheckBox *ckNumber;
	TCheckBox *ckSpec;
	TNumberBox *edLenght;
	TLabel *laLenght;
	TCheckBox *ckSmiles;
	void __fastcall buPasswordClick(TObject *Sender);
	void __fastcall edLenghtChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
