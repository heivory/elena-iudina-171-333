//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "uUtils.h" // <---------
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPasswordClick(TObject *Sender)
{
	edPassword->Text = RandomStr(StrToIntDef(edLenght->Text,9),
		ckLower->IsChecked, ckUpper->IsChecked, ckNumber->IsChecked,
		ckSpec->IsChecked, ckSmiles->IsChecked);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::edLenghtChange(TObject *Sender)
{
	if (edLenght->Value > 10) {

		ShowMessage ("Please, ne bol'she 10");
        edLenght->Value = 10;

	}
}
//---------------------------------------------------------------------------
