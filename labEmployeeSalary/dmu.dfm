object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 322
  Width = 421
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Program Files\Firebird\Firebird_3_0\examples\empbuil' +
        'd\EMPLOYEE.FDB'
      'User_Name=SYSDBA'
      'CharacterSet=UTF8'
      'Password=masterkey'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 264
    Top = 16
  end
  object FDTable1: TFDTable
    IndexFieldNames = 'EMP_NO'
    Connection = FDConnection1
    UpdateOptions.UpdateTableName = 'EMPLOYEE'
    TableName = 'EMPLOYEE'
    Left = 40
    Top = 16
  end
  object quEmployee: TFDQuery
    Active = True
    AfterScroll = quEmployeeAfterScroll
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    employee.emp_no,'
      '    employee.last_name,'
      '    employee.first_name,'
      '    employee.phone_ext,'
      '    employee.hire_date,'
      '    employee.salary,'
      '    employee.dept_no,'
      '    department.department'
      'from department'
      
        '   inner join employee on (department.dept_no = employee.dept_no' +
        ')'
      'order by employee.first_name, employee.last_name')
    Left = 152
    Top = 16
    object quEmployeeEMP_NO: TSmallintField
      FieldName = 'EMP_NO'
      Origin = 'EMP_NO'
      Required = True
    end
    object quEmployeeLAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Origin = 'LAST_NAME'
      Required = True
    end
    object quEmployeeFIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
      Origin = 'FIRST_NAME'
      Required = True
      Size = 15
    end
    object quEmployeePHONE_EXT: TStringField
      FieldName = 'PHONE_EXT'
      Origin = 'PHONE_EXT'
      Size = 4
    end
    object quEmployeeHIRE_DATE: TSQLTimeStampField
      FieldName = 'HIRE_DATE'
      Origin = 'HIRE_DATE'
      Required = True
    end
    object quEmployeeSALARY: TFMTBCDField
      FieldName = 'SALARY'
      Origin = 'SALARY'
      Required = True
      Precision = 18
      Size = 2
    end
    object quEmployeeDEPT_NO: TStringField
      FieldName = 'DEPT_NO'
      Origin = 'DEPT_NO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      FixedChar = True
      Size = 3
    end
    object quEmployeeDEPARTMENT: TStringField
      FieldName = 'DEPARTMENT'
      Origin = 'DEPARTMENT'
      Required = True
      Size = 25
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 344
    Top = 216
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 344
    Top = 264
  end
  object quSalaryHistory: TFDQuery
    Active = True
    Filtered = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    salary_history.emp_no,'
      '    salary_history.change_date,'
      '    salary_history.old_salary,'
      '    salary_history.new_salary'
      'from salary_history'
      'order by salary_history.change_date')
    Left = 40
    Top = 80
    object quSalaryHistoryEMP_NO: TSmallintField
      FieldName = 'EMP_NO'
      Origin = 'EMP_NO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSalaryHistoryCHANGE_DATE: TSQLTimeStampField
      FieldName = 'CHANGE_DATE'
      Origin = 'CHANGE_DATE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quSalaryHistoryOLD_SALARY: TFMTBCDField
      FieldName = 'OLD_SALARY'
      Origin = 'OLD_SALARY'
      Required = True
      Precision = 18
      Size = 2
    end
    object quSalaryHistoryNEW_SALARY: TFloatField
      FieldName = 'NEW_SALARY'
      Origin = 'NEW_SALARY'
    end
  end
end
